pub struct House {
	walls: String,
	roof: String,
	windows: i8,
}

impl House {
	pub fn check(&self) {
		println!("This is my house: {:?} walls, {:?} roof, {:?} windows...", self.walls, self.roof, self.windows);
	}
}

pub struct HouseBuildDirector {
	pub builder: Box<dyn HouseBuilder>
}

impl HouseBuildDirector {
	pub(crate) fn build(&mut self) {
		self.builder.build_walls();
		self.builder.build_roof();
		self.builder.add_windows();
	}
}

pub trait HouseBuilder {
	fn build_walls(&mut self);

	fn build_roof(&mut self);

	fn add_windows(&mut self);

	fn get_result(&self) -> House;
}

pub struct DefaultHouseBuilder {
	pub walls: Option<String>,
	pub roof: Option<String>,
	pub windows: Option<i8>,
}

impl Default for DefaultHouseBuilder {
	fn default() -> DefaultHouseBuilder {
		DefaultHouseBuilder {
			walls: None,
			roof: None,
			windows: None
		}
	}
}

impl HouseBuilder for DefaultHouseBuilder {
	fn build_walls(&mut self) {
		self.walls = Some(String::from("wood"));
	}

	fn build_roof(&mut self) {
		self.roof = Some(String::from("wood"));
	}

	fn add_windows(&mut self) {
		self.windows = Some(2);
	}

	fn get_result(&self) -> House {
		let walls = self.walls.clone().unwrap_or(String::from("no"));
		let roof = self.roof.clone().unwrap_or(String::from("no"));
		let windows = self.windows.unwrap_or(0);

		House {walls, roof, windows}
	}
}

pub struct RichHouseBuilder {
	pub walls: Option<String>,
	pub roof: Option<String>,
	pub windows: Option<i8>,
}

impl HouseBuilder for RichHouseBuilder {
	fn build_walls(&mut self) {
		self.walls = Some(String::from("briliant"));
	}

	fn build_roof(&mut self) {
		self.roof = Some(String::from("gold"));
	}

	fn add_windows(&mut self) {
		self.windows = Some(6);
	}

	fn get_result(&self) -> House {
		let walls = self.walls.clone().unwrap_or(String::from("no"));
		let roof = self.roof.clone().unwrap_or(String::from("no"));
		let windows = self.windows.unwrap_or(0);

		House {walls, roof, windows}
	}
}

impl Default for RichHouseBuilder {
	fn default() -> RichHouseBuilder {
		RichHouseBuilder {
			walls: None,
			roof: None,
			windows: None
		}
	}
}
