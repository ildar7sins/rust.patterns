use std::sync::{Mutex};
use std::time::{SystemTime, UNIX_EPOCH};
// use std::sync::atomic::Ordering;

lazy_static! {
	static ref MY_SINGLETON: MySingleton = MySingleton {
		value: Mutex::new(SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs()),
	};
}

pub struct MySingleton {
	value: Mutex<u64>,
}

impl MySingleton {
	pub fn get_value() -> u64 {
		*MY_SINGLETON.value.lock().unwrap()
	}

	pub fn add(n: u64) {
		let mut value = MY_SINGLETON.value.lock().unwrap();
		*value += n;
	}
}

