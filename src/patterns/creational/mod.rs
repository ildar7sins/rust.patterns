// Creational patterns...
pub mod factory_method;
pub mod abstract_factory;
pub mod builder;
pub mod prototype;
pub mod singleton;
