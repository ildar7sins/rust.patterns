

pub trait FigurePrototype {
	fn clone(&self) -> Self;
}

#[derive(Debug)]
pub struct Shape {
	pub title: String,
	pub x: f32,
	pub y: f32,
	pub height: i32,
	pub width: i32,
}

impl FigurePrototype for Shape {
	fn clone(&self) -> Shape {
		Shape {
			title: self.title.clone(),
			x: self.x,
			y: self.y,
			width: self.width,
			height: self.height,
		}
	}
}

