
#[derive(Debug)]
pub enum Delivery {
	Sea,
	Road
}

/* ################################# Transport interface ################################ */
pub trait Transport {
	fn deliver(&self);
}

/* ################################### Transport types ################################## */
pub struct Truck {
	number: String,
	delivery_type: Delivery
}
impl Transport for Truck {
	fn deliver(&self)
	{
		println!("Start deliver by {:?}, number: {}...", self.delivery_type, self.number);
		println!("Delivering...");
		println!("Delivered to the warehouse!");
	}
}

pub struct Ship {
	number: String,
	delivery_type: Delivery
}
impl Transport for Ship {
	fn deliver(&self)
	{
		println!("Start deliver by {:?}, number: {}...", self.delivery_type, self.number);
		println!("Delivering...");
		println!("Delivered to the port!");
	}
}

/* ################################## Factory interface ################################# */
pub trait TransportFactory {
	fn factory_method(&self) -> Box<dyn Transport>;

	fn init_delivery(&self)
	{
		let transport = self.factory_method();
		transport.deliver();
	}
}

/* #################################### Factory types ################################### */
pub struct TruckFactory {}
impl TransportFactory for TruckFactory {
	fn factory_method(&self) -> Box<dyn Transport>
	{
		Box::new(Truck {
			number: "e342ap".to_string(),
			delivery_type: Delivery::Road
		})
	}
}

pub struct ShipFactory {}
impl TransportFactory for ShipFactory {
	fn factory_method(&self) -> Box<dyn Transport>
	{
		Box::new(Ship {
			number: "342-af-3".to_string(),
			delivery_type: Delivery::Sea
		})
	}
}
