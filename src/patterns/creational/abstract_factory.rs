
/* ################################ Furniture interfaces ################################ */
pub trait Table {
	fn put_on(&self);
}

pub trait Chair {
	fn sit_on(&self);
}

/* ################################### Furniture types ################################## */
struct VictorianChair {}
impl Chair for VictorianChair {
	fn sit_on(&self) {
		println!("Sit on victorian chair...");
	}
}

struct VictorianTable {}
impl Table for VictorianTable {
	fn put_on(&self) {
		println!("Put on victorian table...");
	}
}

struct ModernChair {}
impl Chair for ModernChair {
	fn sit_on(&self) {
		println!("Sit on modern chair...");
	}
}

struct ModernTable {}
impl Table for ModernTable {
	fn put_on(&self) {
		println!("Put on modern table...");
	}
}

/* ################################# Factory interface ################################## */
pub trait AbstractFurnitureFactory {
	fn create_table(&self) -> Box<dyn Table>;
	fn create_chair(&self) -> Box<dyn Chair>;
}

/* ############################### Furniture factory types ############################## */
pub struct VictorianFurnitureFactory {
	// Some data..
}

impl AbstractFurnitureFactory for VictorianFurnitureFactory {
	fn create_table(&self) -> Box<dyn Table> {
		Box::new(VictorianTable{})
	}

	fn create_chair(&self) -> Box<dyn Chair> {
		Box::new(VictorianChair{})
	}
}

pub struct ModernFurnitureFactory {
	// Some data..
}

impl AbstractFurnitureFactory for ModernFurnitureFactory {
	fn create_table(&self) -> Box<dyn Table> {
		Box::new(ModernTable{})
	}

	fn create_chair(&self) -> Box<dyn Chair> {
		Box::new(ModernChair{})
	}
}
