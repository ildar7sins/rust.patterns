// Structural patterns...
pub mod adapter;
pub mod bridge;
pub mod composite;
pub mod decorator;
pub mod facade;
pub mod flyweigh;
pub mod proxy;
