use std::thread::sleep;
use std::time::Duration;
use crate::patterns::creational::factory_method::{TransportFactory, TruckFactory, ShipFactory};
use crate::patterns::creational::abstract_factory::{ModernFurnitureFactory, VictorianFurnitureFactory, AbstractFurnitureFactory};
use crate::patterns::creational::singleton::MySingleton;
use crate::patterns::creational::builder::{HouseBuildDirector, HouseBuilder, DefaultHouseBuilder, RichHouseBuilder, House};

use termion::{color, style};
use crate::patterns::creational::prototype::{Shape, FigurePrototype};

#[macro_use]
extern crate lazy_static;

mod patterns;

fn main() {
    show_header("Creational patterns");
    creational();
}

pub fn creational()
{
    show_header("Factory method");
    let some_factory_1 = TruckFactory{};
    run_factory_method(some_factory_1);

    println!("\n");
    let some_factory_2 = ShipFactory{};
    run_factory_method(some_factory_2);

    show_header("Abstract factory");
    run_abstract_factory(VictorianFurnitureFactory{});
    println!("\n");
    run_abstract_factory(ModernFurnitureFactory{});

    show_header("Builder");
    run_builder(Box::new(DefaultHouseBuilder{..DefaultHouseBuilder::default()}));
    run_builder(Box::new(RichHouseBuilder{..RichHouseBuilder::default()}));

    show_header("Prototype");
    run_prototype();

    show_header("Singleton");
    println!("Value is {}", MySingleton::get_value());
    sleep(Duration::new(2, 0));
    println!("Value after 2 sec is {}", MySingleton::get_value());
    MySingleton::add(1);
    println!("Value after increment on 1 is {}", MySingleton::get_value());
}

pub fn run_factory_method(factory: impl TransportFactory)
{
    factory.init_delivery();
}

pub fn run_abstract_factory(factory: impl AbstractFurnitureFactory)
{
    let table = factory.create_table();
    let chair = factory.create_chair();

    table.put_on();
    chair.sit_on();
}

pub fn run_builder(builder: Box<dyn HouseBuilder>)
{
    let mut director = HouseBuildDirector{builder};
    director.build();
    let house: House = director.builder.get_result();
    house.check();
}

pub fn run_prototype()
{
    let circle = Shape {
        title: String::from("circle"),
        x: 1.0,
        y: 1.6,
        width: 20,
        height: 20
    };
    let second_circle: Shape = circle.clone();

    println!("First circle: {:?}", circle);
    println!("Second circle: {:?}", second_circle);
}

pub fn show_header(header: &str)
{
    println!("\n{}{}################# {} #################{}{}",
             style::Bold, color::Fg(color::Yellow),
             header,
             style::Reset, color::Fg(color::White)
    );
}
